﻿using System;

namespace game
{
    class Program
    {
        private  static Random rnd = new Random();

        private static int heroNumber;
        private static int[] heroLife;
        private static int[] heroDamage;
        private static int[] heroArmor;

        private static int monsterNumber;
        private static int[] monsterDamage;
        private static int[] monsterLife;

        private static double [] monsterXp;

        private static string[] heroNames = 
        {
            "Godji",
            "AntonyAntonyQ",
            "Pio Nekros kai apo ton thanato",
            "Manas",
            "Olakala mexri pou mpikes esu",
            "Asa Akira",
            "Hitler",
            "Big Dick",
            "Small but fat dick",
            "Just a guy",
            "Pantelis Pantelidis",
            "Ninja",
        };
        private static string[] monsterNames = 
        {
            "Mana Karalh",
            "Karalhs",
            "PENTAPODO LIONTARI",
            "ADERFI KARALH",
            "Gkomena KARALH",
            "ZAFIRIS",
            "ASXIMOS GEROS KARALH",
            "ASXIMOS GYTONAS KARALH",
            "KEFTEDAKIA",
            "MPAMIES",
            "ola mesa",
            "den exw allh fantasia",
        };

        private static Hero [] heros ;
        private static Monster [] monsters;

        static void Main(string[] args)
        {
            Console.WriteLine(toString() + "game");

            initializeDammyCaracters();

            setHerosNMonsters(Program. heroLife,Program. heroDamage, Program. heroArmor, Program. monsterLife, Program. monsterDamage, Program. monsterXp);
            game();

        }

        public static void initializeDammyCaracters(){
            Program.heroNumber = rnd.Next(1, 13);  
            Console.WriteLine("On this round we will have : " + Program.heroNumber + " heros");
            Program.heros = new Hero[Program.heroNumber];

            Program.monsterNumber = rnd.Next(1, 13);
            Console.WriteLine("On this round we will have : " + Program.monsterNumber + " monsters");
            Program.monsters = new Monster[Program.monsterNumber];

            //initialize heros & monsters dummy data
             Program.heroArmor = new int[Program.heroNumber];
             Program.heroDamage = new int[Program.heroNumber];
             Program.heroLife = new int[Program.heroNumber];
             Program.monsterDamage = new int[Program.monsterNumber];
             Program.monsterLife = new int[Program.monsterNumber];
            Program.monsterXp = new double[Program.monsterNumber];
            for(int i = 0; i<Program.heroNumber; i++){
                Program.heroArmor[i] = rnd.Next(1, 100);
                Program.heroDamage[i] = rnd.Next(1, 100);
                Program.heroLife[i] = rnd.Next(100, 1000);
            }

            for(int i = 0; i<Program.monsterNumber; i++){
                Program.monsterLife[i] = rnd.Next(100, 1000);
                Program.monsterDamage[i] = rnd.Next(1, 100);
                Program.monsterXp[i] = rnd.Next(100, 1000)/rnd.Next(1, 100);
            }
        }

        public static void setHerosNMonsters(int[] heroLife, int[] heroDamage, int[] heroArmor, int[] monsterLife, int[] monserDamage, double[] monsterXP){
            validateData(heroLife, heroDamage ,heroArmor, monsterLife , monserDamage, monsterXP);
            
            heros = new Hero[heroLife.Length];
            monsters = new Monster[monsterLife.Length];

            for(int i=0; i<heroLife.Length; i++){
                heros[i] = new Hero(heroNames[i] , heroLife[i], heroDamage[i], heroArmor[i]);    
            }
            for(int i=0; i<monsterLife.Length; i++){
                monsters[i] = new Monster(monsterNames[i], monsterLife[i], monserDamage[i], monsterXP[i]);
            }
        }

        private static void validateData(int[] heroLife, int[] heroDamage, int[] heroArmor, int[] monsterLife, int[] monserDamage, double[] monsterXP){
            if( ! ( heroLife.Length == heroDamage.Length && heroDamage.Length == heroArmor.Length)){
                throw new WrongDataTableLength("The 3 tables refair to hero must have identical length.");
            }
            if( ! ( monsterLife.Length == monsterDamage.Length && monserDamage.Length == monsterXP.Length)){
                throw new WrongDataTableLength("The 3 tables refair to monster must have identical length.");
            }
        }

        
        static  int current_monster = 0;
        static  int  current_hero = 0;
        
        public static void herosWins(){
             Console.WriteLine("COntgrats Heros Wins");
             Console.WriteLine("Here are the XP that the alive heros have gathered for their try :");
             for(int i =current_hero; i<heros.Length; i++){
                 Console.WriteLine("Hero : " + heros[i].getHeroName() +" has gathered : " + heros[i].getHeroXP() +" xp");
             }
        }
        public static void monsrersWin(){
             Console.WriteLine("COntgrats monsrers Wins");
        }
        
        private static  void game(){
            Console.WriteLine("************************************* Heros turn to Attack ! **************************************");
        
            for(int i = current_hero; i<heros.Length; i++){
                if(!(monsters[Program.current_monster].getMonsterLife() > 0)){
                    Console.WriteLine("Monster : " + monsters[current_monster].getMonsterName() + " is dead.");
                    if(current_monster >= monsters.Length-1){
                        herosWins();
                        return;
                    }else{
                        current_monster++;
                    }
                }
                if( !monsters[current_monster].getDamage(heros[i].getHeroDamage())){
                    Console.WriteLine("Monster : " + monsters[current_monster].getMonsterName() + " has fallen.");
                    Console.WriteLine("Hero : " + heros[i].getHeroName() + " has gained : "+ monsters[current_monster].getMonsterXp());
                    heros[i].addXP(monsters[current_monster].getMonsterXp());
                    if(current_monster >= monsters.Length -1 ){
                        herosWins();
                        return;
                    }else{
                        current_monster++;
                    }
                }

            }
            Console.WriteLine("************************************* Monster turn to Attack ! **************************************");

            for(int i = current_monster; i< monsters.Length; i++){        
                
                if(!(heros[Program.current_hero].getHerLifePoints() > 0)){
                    Console.WriteLine("Hero : " + heros[current_hero].getHeroName() + " has fallen.");
                    if(current_hero >= heros.Length -1){
                        monsrersWin();
                        return;
                    }else{
                        current_hero ++;
                    }
                }
                if( !heros[current_hero].damageHero(monsters[i].dealDamange())){
                    if(current_hero >= heros.Length -1){
                        monsrersWin();
                        return;
                    }else{current_hero++;}
                }

            }

            if (current_monster < monsters.Length && current_hero < heros.Length){
                game();
            }else if(current_monster >= monsters.Length -1){
                herosWins();
            }else if(current_hero >= heros.Length -1){
                monsrersWin();
            }
        }

        private static string toString()
        {
            return "Heroic Last Stand";
        }
    }
    
     public class WrongDataTableLength : Exception
    {
        public WrongDataTableLength()
        {
        }

        public WrongDataTableLength(string message)
            : base(message)
        {
        }

        public WrongDataTableLength(string message, Exception inner)
            : base(message, inner)
        {
        }
    }



}
