public class Monster
    {

        private string monsterName;

        private int lifePoints;

        private double xp;

        private int mosnterDamage;

        private string toString()
        {
            return "I am a Monster, my name is : " + monsterName;
        }

        public Monster(string monsterName, int lifePoints, int mosnterDamage, double xp){
            this.monsterName  = monsterName;
            this.lifePoints = lifePoints;
            this.mosnterDamage = mosnterDamage;
            this.xp = xp;
        }

        public Monster( int lifePoints, int mosnterDamage){
            this.lifePoints = lifePoints;
            this.mosnterDamage = mosnterDamage;
        }
  
        public void setMonsterName(string name){
            this.monsterName = name;
        }
        public void setMonsterXP(double xp){
            this.xp = xp;
        }

         public bool getDamage(int damage){
            this.lifePoints = this.lifePoints - damage;
            return (this.lifePoints > 0 ? true : false);
        }

        public int getMonsterLife(){
            return this.lifePoints;
        }

        public double getMonsterXp(){
            return this.xp;
        }
        
        public string getMonsterName(){
            return this.monsterName;
        }

        public int dealDamange(){
            return this.mosnterDamage;
        }
    }