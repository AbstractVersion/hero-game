public class Hero
    {

        private string heroName;
        private int lifePoints;

        private int armor;

        private int heroDamage;

        private double xp_gained;

        public Hero(string name , int lifep, int damage, int armor){
            this.lifePoints = lifep;
            this.heroName = name;
            this.heroDamage = damage;
            this.armor = armor;
            this.xp_gained =0;
        }
        
        public Hero(string name , int lifep, int damage){
            this.lifePoints = lifep;
            this.heroName = name;
            this.heroDamage = damage;
            this.xp_gained =0;
        }

        public bool damageHero(int damange){
            this.lifePoints = (this.lifePoints - (damange - (int) (this.armor * 0.3)));
            return (this.lifePoints > 0 ? true : false);
        }

        public void addXP(double xp){
            this.xp_gained += xp;
        }

        public double getHeroXP(){
            return this.xp_gained;
        }

        public string getHeroName(){
            return this.heroName;
        }
        public int getHeroDamage(){
            return this.heroDamage;
        }
        
        public int getHerLifePoints(){
            return this.heroDamage;
        }

        private string toString()
        {
            return "I am a Hero, my name is : " + heroName;
        }
    }